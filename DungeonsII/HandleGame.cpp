#include "HandleGame.h"
#include"SFML\Graphics\Text.hpp"
#include"view.h"
#include "Global.h"

HandleGame::HandleGame() : _window2D(VideoMode(_windowWidth, _windowHeight), "DangII"), _window3D(VideoMode(_windowWidth, _windowHeight), "DangI" ), _map(&_window2D), _player(_window2D, "player", 1, 1, Vector2f(5 * _TileSize, 3 * _TileSize) , Player1)
{


	//_enemies.resize(_map->getEnemyCount());
	_window2D.setPosition({ 800, 100 });
    _window3D.setPosition({ 150,150 });

	_window3D.setMouseCursorVisible(false);
	_view.reset(sf::FloatRect(0, 0, 600, 400));
	
}

void HandleGame::initGame()
{
	vector<pair<Vector2i, UnitSignature>> units;
	_map.loadMap(0, units);
	initUnitFromMap(units);
}

void HandleGame::HGHC(Vector2f &dPos)
{
	if (!Keyboard::isKeyPressed(Keyboard::P))
	{
		if (Keyboard::isKeyPressed(Keyboard::W) || Keyboard::isKeyPressed(Keyboard::A) || Keyboard::isKeyPressed(Keyboard::S) || Keyboard::isKeyPressed(Keyboard::D))
		{
			float dx, dy;
			float mx, my;
			int ang = _player.getAngle();

			dx = _TileSize * cos(ang * 3.14 / 180) ;
			dy = _TileSize * sin(ang * 3.14 / 180) ;

			mx = _TileSize * cos((ang + 90) * 3.14 / 180);
			my = _TileSize * sin((ang + 90) * 3.14 / 180);

			if (Keyboard::isKeyPressed(Keyboard::W))
			{
				dPos.x = dx;
				dPos.y = dy;
			}
			if (Keyboard::isKeyPressed(Keyboard::S))
			{
				dPos.x = -dx;
				dPos.y = -dy;
			}
			if (Keyboard::isKeyPressed(Keyboard::A))
			{
				dPos.x = -mx;
				dPos.y = -my;
			}
			if (Keyboard::isKeyPressed(Keyboard::D))
			{
				dPos.x = mx;
				dPos.y = my;
			}
			if (Keyboard::isKeyPressed(Keyboard::LAlt))
			{
				dPos.x *= 5;
				dPos.y *= 5;
			}
		}
		else
		{
			dPos = Vector2f(0, 0);
		}
		if (abs(Mouse::getPosition(_window3D).x - int(_windowWidth / 2)) > 5)
		{
			_player.setAngle((Mouse::getPosition(_window3D).x - int(_windowWidth / 2)) / 5);
		}
		Vector2i pos;
		pos.x = int(_windowWidth / 2);
		pos.y = int(_windowHeight / 2);
		Mouse::setPosition(pos, _window3D);
	}

}

void HandleGame::draw3DQuad()
{
	sf::VertexArray quad(sf::Quads, 4);


	quad[0].position = sf::Vector2f(0, 0);
	quad[1].position = sf::Vector2f(_windowWidth, 0);
	quad[2].position = sf::Vector2f(_windowWidth, _windowHeight / 2);
	quad[3].position = sf::Vector2f(0, _windowHeight / 2);

	quad[0].color = Color(130, 130, 130);
	quad[1].color = Color(130, 130, 130);
	quad[2].color = Color(130, 130, 130);
	quad[3].color = Color(130, 130, 130);
	_window3D.draw(quad);
}

void HandleGame::draw3DPricel()
{
	sf::VertexArray pricel(sf::Lines, 4);
	pricel[0].position = sf::Vector2f(_windowWidth / 2 - 20, _windowHeight / 2);
	pricel[1].position = sf::Vector2f(_windowWidth / 2 + 20, _windowHeight / 2);

	pricel[2].position = sf::Vector2f(_windowWidth / 2, _windowHeight / 2 - 20);
	pricel[3].position = sf::Vector2f(_windowWidth / 2, _windowHeight / 2 + 20);
	pricel[0].color = Color(150, 255, 0);
	pricel[1].color = Color(150, 255, 0);
	pricel[2].color = Color(150, 255, 0);
	pricel[3].color = Color(150, 255, 0);
	_window3D.draw(pricel);
}

void HandleGame::initUnitFromMap(const vector<pair<Vector2i, UnitSignature>>& units)
{
	for (const auto &data : units)
	{
		cout << "X:" << data.first.x << " y:" << data.first.y << " Type:" << data.second << std::endl;
		_enemies.push_back(Enemy(_window2D,"Enemy",1,1,MapPosToScreenPos(data.first),data.second));
	}

}
void HandleGame::putUnitOnMap() 
{
	for ( Enemy &enemy : _enemies)
	{
		_map.putUnit(ScreenPosToMapPos(enemy.getPosition()), enemy.getUnitSignature());
	}
	

}
void HandleGame::removeUnitFromMap()
{
	for (Enemy &enemy : _enemies)
	{
		_map.putUnit(ScreenPosToMapPos(enemy.getPosition()), Parket);
	}
}

void HandleGame::moveBots()
{
	Vector2f deltaPos;
	for (Enemy &enemy : _enemies)
	{
		enemy.move(deltaPos);
		if (_map.checkPos(enemy.deltaPlusPos(deltaPos)))
		{
			enemy.setPosition(enemy.deltaPlusPos(deltaPos));
		}
	}

}

void HandleGame::game()
{
	//***************************************************************************
	//***************************************************************************
	//***************************************************************************

	Text text1 , text2;
	Font font;
	if (!(font.loadFromFile("C:\\windows\\fonts\\arial.ttf")))
	{
		cout << "Error loading font " << std::endl;
	}
	text1.setCharacterSize(20);
	//_text1.setScale({100,70});
//	_text1.setColor(Color(100, 50, 30));
	text1.setFont(font);
	text1.setPosition({50 , 20 });

	text2.setCharacterSize(20);
	//_text2.setScale({ 100,70 });
	//text2.setColor(Color(100, 50, 70));
	text2.setFont(font);
	text2.setPosition({ 50 , 50 });

	text1.setString("PAUSE - P");
	text2.setString("EXIT - H");

	initGame();
	//***************************************************************************
	//***************************************************************************
	//***************************************************************************

	Vector2f deltaPos;

	while ((_window2D.isOpen())&&(_window3D.isOpen()))
	{

		while (_window2D.pollEvent(_event)&&(_window3D.pollEvent(_event)))
		{
			if (_event.type == Event::Closed)
			{
				_window2D.close();
				_window3D.close();
			}
		}
		
		HGHC(deltaPos);


		//_player.move(_window3D);

		if (_map.checkPos(_player.deltaPlusPos(deltaPos)))
		{
			_player.setPosition(_player.deltaPlusPos(deltaPos));
		}

		removeUnitFromMap();
		moveBots();
		putUnitOnMap();


		_window3D.clear(Color(80,80,80));
		_window2D.clear();
		draw3DQuad();


		
		if (Keyboard::isKeyPressed(Keyboard::P))
		{
			_window3D.setMouseCursorVisible(true);
			text1.setString("CONTINUE - L");
		//	_text1.setColor(Color(30, 50, 100));
			_window3D.draw(text1);
			_window3D.display();
			while (!(Keyboard::isKeyPressed(Keyboard::L)))
			{
				
			}
			text1.setString("PAUSE - P");
//			text1.setColor(Color(100, 50, 30));
			_window3D.setMouseCursorVisible(false);
		}
		if (Keyboard::isKeyPressed(Keyboard::H))
		{
			_window2D.close();
			_window3D.close();
		} 

		my_setViewPosition(_player.getPosition());
		changeview();
		_window2D.setView(_view);

		_map.drawMap();

		_window3D.draw(text1);
		_window3D.draw(text2);
		
		
		_map.rayCast(_window3D,_player.getPosition(),_player.getAngle());


		draw3DPricel();
		_window2D.display();
		_window3D.display();
		
		
		sf::sleep(sf::milliseconds(1000/50));

	}
}

HandleGame::~HandleGame()
{
	
}
