#include "unit.h"

Unity::Unity(RenderWindow &window2D ,String name, int unitWidth, int unitHeight, Vector2f pos, UnitSignature unitSignature)
{
	_window2D = &window2D;
	string partName;
	Image image;
	_unitWidth = unitWidth;
	_unitHeight = unitHeight;
	_unitName = name;
	_unitSignature = unitSignature;
	_isAlive = true;

	partName = _Directory + "units\\" + name + ".png";
	image.loadFromFile(partName);
	_unitTexture.loadFromImage(image);
	_unitSprite.setTexture(_unitTexture);
	_unitSprite.setTextureRect(IntRect(0, 0, unitWidth* _TileSize , unitHeight* _TileSize));
	_unitSprite.setPosition(pos);

	partName = _Directory + "media\\" + name + "_deathSound" + ".ogg";
	_sbuf.loadFromFile(partName);
	_deathSound.setBuffer(_sbuf);

	partName = _Directory + "media\\" + name + "_walkingSound" + ".ogg";
	_sbuf.loadFromFile(partName);
	_walkingSound.setBuffer(_sbuf);

	partName = _Directory + "media\\" + name + "_attackSound" + ".ogg";
	_sbuf.loadFromFile(partName);
	_attackSound.setBuffer(_sbuf);

	if (name == "player")
	{
		_health = 100;
		_damage = 20;
	}
	if (name == "low-dif")
	{
		_health = 40;
		_damage = 20;
	}
	if (name == "med-dif")
	{
		_health = 80;
		_damage = 30;
	}
	if (name == "high-dif")
	{
		_health = 200;
		_damage = 40;
	}


}

void Unity::move()
{
}

void Unity::draw2D()
{
	/*sf::VertexArray quad(sf::Quads, 4);

	float x = _unitSprite.getPosition().x;
	float y = _unitSprite.getPosition().y;

	quad[0].position = sf::Vector2f(x, y);
	quad[1].position = sf::Vector2f(x + _TileSize, y);
	quad[2].position = sf::Vector2f(x + _TileSize, y + _TileSize);
	quad[3].position = sf::Vector2f(x, y+_TileSize);

	_window2D->draw(quad);*/

	_window2D->draw(_unitSprite);
}

Vector2f Unity::deltaPlusPos(Vector2f dPos)
{
	return getPosition() + dPos;
}

void Unity::setPosition(Vector2f position)
{
	_unitSprite.setPosition(position);
}

void Unity::setAngle(int ang)
{
	_unitSprite.rotate(ang);
}

UnitSignature Unity::getUnitSignature()
{
	return _unitSignature;
}

Vector2f Unity::getPosition()
{
	return _unitSprite.getPosition();
}
Vector2i Unity::getPositionMap()
{
	Vector2i pos;
	pos.x = getPosition().x/ _TileSize;
	pos.y = getPosition().y/ _TileSize;
	return pos;
}

int Unity::getAngle()
{
	return _unitSprite.getRotation();
}
