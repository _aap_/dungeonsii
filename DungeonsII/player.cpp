#include "player.h"

Player::Player(RenderWindow &window2D ,String name, int unitWidth, int unitHeight, Vector2f pos , UnitSignature unitSignature):Unity( window2D , name,  unitWidth,  unitHeight,  pos , unitSignature)
{

}

void Player::move(RenderWindow &window)
{
	float x = _unitSprite.getPosition().x;
	float y = _unitSprite.getPosition().y;
	float dx, dy;
	float mx, my;
	int ang = _unitSprite.getRotation();

	dx = _TileSize * cos(ang * 3.14 / 180) / 4;
	dy = _TileSize * sin(ang * 3.14 / 180) / 4;

	mx = _TileSize * cos((ang + 90) * 3.14 / 180) / 2;
	my = _TileSize * sin((ang + 90) * 3.14 / 180) / 2;

	if (Keyboard::isKeyPressed(Keyboard::W) && Keyboard::isKeyPressed(Keyboard::LAlt))
		_unitSprite.setPosition(x + dx * 5, y + dy * 5);
	else if (Keyboard::isKeyPressed(Keyboard::W))
		_unitSprite.setPosition(x + dx, y + dy);
	if (Keyboard::isKeyPressed(Keyboard::S))
		_unitSprite.setPosition(x - dx, y - dy);
	if (Keyboard::isKeyPressed(Keyboard::A))
		_unitSprite.setPosition(x - mx, y - my);

	if (Keyboard::isKeyPressed(Keyboard::D))
		_unitSprite.setPosition(x + mx, y + my);
	if (abs(Mouse::getPosition(window).x - int(_windowWidth / 2)) > 5)
	{
		_unitSprite.rotate((Mouse::getPosition(window).x - int(_windowWidth / 2)) / 5);
	}
	Vector2i pos;
	pos.x = int(_windowWidth / 2);
	pos.y = int(_windowHeight / 2);
	Mouse::setPosition(pos ,window);

}
