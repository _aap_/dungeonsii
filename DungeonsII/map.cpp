#include "map.h"
#include <math.h>



Map::Map(RenderWindow *window)
{
	_enemyCount = 0;
	_curLocNum = 4;
	_curLvlNum = 1;
	//****************************************************************************************
	_opener = _Directory + "Levels"+"\\" + "level" + to_string(_curLvlNum) + "\\" + "map.png";//�������� ����� � ������� ������ SUKA
	cout <<"Texture "<<_opener << endl;
	_mapImage.loadFromFile(_opener);
	_mapTexture.loadFromImage(_mapImage);
	_mapSprite.setTexture(_mapTexture);
	//****************************************************************************************
	_opener = _Directory + "Levels" + "\\" + "count.txt";
	_file.open(_opener);
	s_reader = "";
	while (_file.read((char*)&_reader, sizeof(_reader)))
	{
		s_reader = s_reader + _reader;
	}
	_lvlCount = atoi(s_reader.c_str());
	_file.close();
	//----------------------------------//----------------------------------//----------------------------------
	_opener = _Directory + "Levels" + "\\" + "level" + to_string(_curLvlNum) + "\\" + "count.txt";
	_file.open(_opener);
	s_reader = "";
	while (_file.read((char*)&_reader, sizeof(_reader)))
	{
		s_reader = s_reader + _reader;
	}
	_locCount = atoi(s_reader.c_str());
	_file.close();
	//****************************************************************************************
	_window = window;
}

void Map::setLevel(int levelNum)
{
	_curLvlNum = levelNum;
}

void Map::setLocation(int locationNum)
{
	_curLocNum = locationNum;
}

void Map::LocationCounter()
{
	_opener = _Directory + "Levels" + "\\" + "level" + to_string(_curLvlNum) + "\\" + "count.txt";
	_file.open(_opener);
	s_reader = "";
	while (_file.read((char*)&_reader, sizeof(_reader)))
	{
		s_reader = s_reader + _reader;
	}
	_locCount = atoi(s_reader.c_str());
	_file.close();
}


void Map::loadMap(int del, vector<pair<Vector2i, UnitSignature>> &units)
{
	int i = 0 ;
	_curLocNum = _curLocNum + del;
	if (_curLocNum > _locCount)
	{
		_curLocNum = 1;
		_curLvlNum += 1;
		this->LocationCounter();
	}
	if (_curLvlNum > _lvlCount)
	{
		_window->close();
	}
	
	_opener = _Directory + "Levels" + "\\" + "level" + to_string(_curLvlNum) + "\\" + "location" + to_string(_curLocNum) + ".txt";
	_file.open(_opener);
	while (_file.read((char*)&_reader, sizeof(_reader)))
	{
		//cout << _reader;
		if ((_reader != Parket)&&(_reader != Wall)&&(_reader != '\n'))
		{
			_enemyCount += 1;
			_map[i] = _map[i] + ' ';
			
			std::pair<Vector2i,UnitSignature> data(Vector2i(_map[i].length(), i), UnitSignature(_reader));
			//cout << "X: " << _map[i].length() << "Y: " << i << " " << _reader << endl;
			units.push_back(data);
		}
		else
		{
			_map[i] = _map[i] + _reader;
		}

		if (_reader == '\n')
		{
			
			cout << _map[i];
			i++;
		}

		
	}
	_lairsCount = i + 1;
}

void Map::drawMap()
{
	int d = 0, D = 0;
	for (int i = 0; i <= _lairsCount; i++)
		for (int j = 0; j < _map[i].length(); j++)
		{
			//
			//cout << _map[i][j];
			switch (_map[i][j])
			{
			case ' ': {
				_mapSprite.setTextureRect(IntRect(96, 0, 32, 32));
				_mapSprite.setPosition(j * _TileSize, i * _TileSize);
				_window->draw(_mapSprite);//������ ���������� �� �����
									   //i++;
				break;
			}
			case '2': {
				_mapSprite.setTextureRect(IntRect(64, 0, 32, 32));
				_mapSprite.setPosition(j * _TileSize, i * _TileSize);
				_window->draw(_mapSprite);//������ ���������� �� �����
									   //i++;
				break;
			}
			case '1': {
				_mapSprite.setTextureRect(IntRect(32, 0, 32, 32));
				_mapSprite.setPosition(j * _TileSize, i * _TileSize);
				_window->draw(_mapSprite);//������ ���������� �� �����
									   //i++;
				break;
			}
			case '0': {
				_mapSprite.setTextureRect(IntRect(32, 0, 32, 32));
				_mapSprite.setPosition(j * _TileSize, i * _TileSize);
				_window->draw(_mapSprite);//������ ���������� �� �����
										  //i++;
				break;
			}
			case 'e': {
				_mapSprite.setTextureRect(IntRect(0, 0, 32, 32));
				_mapSprite.setPosition(j * _TileSize, i * _TileSize);
				_window->draw(_mapSprite);//������ ���������� �� �����
									   //i++;
				break;
			}
			case 'd': {
				if (d == 0)
				{
					_mapSprite.setTextureRect(IntRect(128, 0, 32, 32));
					_mapSprite.setPosition(j * _TileSize, i * _TileSize);
					_window->draw(_mapSprite);
				}
				if (d == 1)
				{
					_mapSprite.setTextureRect(IntRect(160, 0, 32, 32));
					_mapSprite.setPosition(j * _TileSize, i * _TileSize);
					_window->draw(_mapSprite);
				}
				d = 1;
				break;
			}
			case 'D': {
				if (D == 0)
				{
					_mapSprite.setTextureRect(IntRect(192, 0, 32, 32));
					_mapSprite.setPosition(j * _TileSize, i * _TileSize);
					_window->draw(_mapSprite);
					
				}
				if (D == 1)
				{
					_mapSprite.setTextureRect(IntRect(224, 0, 32, 32));
					_mapSprite.setPosition(j * _TileSize, i * _TileSize);
					_window->draw(_mapSprite);
				}
				D = 1;
				break;
			}
			case '-': {//
				//_mapSprite.setTextureRect(IntRect(128, 0, 32, 32));
				//_mapSprite.setPosition(i * 32, j * 32);
				//_window->draw(_mapSprite);//������ ���������� �� �����
										  //i++;
				break;
			}
			default: {
			
			}
			}
		}


		

	//cout<<endl;
}

void Map::lines(const Vector2f & pos, const int angle)
{
	int i = 0, px = pos.x, py = pos.y, alpha, dx, dy, betta, l;
	VertexArray lines(Lines, 6);

	l = 100;
	betta = angle-30;


	/*lines[0].position = sf::Vector2f(px, py);
	lines[1].position = sf::Vector2f(px + l * cos(betta * 3.14 / 180), py - l * sin(betta * 3.14 / 180));
	lines[2].position = lines[0].position;
	lines[3].position = sf::Vector2f(px + l * cos((betta + _VisionAngle) * 3.14 / 180), py - l * sin((betta + _VisionAngle) * 3.14 / 180));*/
	lines[4].position = pos;
	lines[5].position = sf::Vector2f(pos.x + 2*l*cos((betta + 30) * 3.14 / 180), pos.y + 2 * l*sin((betta + 30) * 3.14 / 180));
	lines[0].color = sf::Color::Green;
	lines[1].color = sf::Color::Green;
	lines[2].color = sf::Color::Green;
	lines[3].color = sf::Color::Red;
	lines[5].color = sf::Color::Red;
	

		/*if (Keyboard::isKeyPressed(Keyboard::A)) {
			betta += 1;
		}
		if (Keyboard::isKeyPressed(Keyboard::D)) {
			betta -= 1;
		}
		if (Keyboard::isKeyPressed(Keyboard::W)) {

			dx = (lines[1].position.x + (lines[3].position.x - lines[1].position.x) / 2);
			dy = (lines[1].position.y + (lines[3].position.y - lines[1].position.y) / 2);

			dx = (lines[0].position.x - dx) / 30;
			dy = (lines[0].position.y - dy) / 30;

			px -= dx;
			py -= dy;
		}
		if (Keyboard::isKeyPressed(Keyboard::S)) {

			dx = (lines[1].position.x + (lines[3].position.x - lines[1].position.x) / 2);
			dy = (lines[1].position.y + (lines[3].position.y - lines[1].position.y) / 2);

			dx = (lines[0].position.x - dx) / 30;
			dy = (lines[0].position.y - dy) / 30;

			px += dx;
			py += dy;
		}*/
	_window->draw(lines);
}

void Map::rayVertical(const Vector2f & pos, const float & sprAngle, float & rayAngle, Vector2f &intersect, float &length)
{
	bool isNull = false;
	int _myRangle = rayAngle;
	float Xa = 0, Ya = 0;
	float gradToRad = 3.14 / 180;
	int Bx = pos.x, By = pos.y;
	
	if ((0 < rayAngle) && (rayAngle < 90))
	{
		By = int(pos.x / _TileSize)*_TileSize + _TileSize;
		Ya = _TileSize ;
		//rayAngle = 180  + rayAngle;
	}
	if ((90 < rayAngle) && (rayAngle < 180))
	{
		By = int(pos.x / _TileSize)*_TileSize - 1;
		Ya = -_TileSize ;
	}
	if ((180 < rayAngle) && (rayAngle < 270))
	{
		By = int(pos.x / _TileSize)*_TileSize - 1;
		Ya = -_TileSize ;
	}
	if ((270 < rayAngle) && (rayAngle < 360))
	{
		By = int(pos.x / _TileSize)*_TileSize + _TileSize;
		Ya = _TileSize ;
		//rayAngle =  180 + rayAngle;
	}
	//��������
	//3
	//4
	//master
	
	if (abs(int(rayAngle)) % 90 == 0)
	{
		isNull = true;
	}
	else
	{
		Bx = pos.y - (pos.x - By) * tan(rayAngle*gradToRad);
		Xa = _TileSize * tan(rayAngle*gradToRad);
	}
	if (!isNull)
	{
		if ((Bx / _TileSize < 0) || (By / _TileSize < 0) || (Bx / _TileSize >= _lairsCount) || (_map[Bx / _TileSize].length() < By / _TileSize))
			isNull = true;
	}
	while ((!isNull) && (_map[Bx / _TileSize][By / _TileSize] == ' '))
	{
		Bx += Xa;
		By += Ya;
		if ((Bx / _TileSize < 0) || (By / _TileSize < 0) || (Bx / _TileSize >= _lairsCount) || (_map[Bx / _TileSize].length() < By / _TileSize))
		isNull = true;
	}
	if (isNull)
	{
		rayAngle = _myRangle;
	}
	else
	{
		//length = abs(Bx - pos.x)*sin(sprAngle*gradToRad) + abs(By - pos.y)*cos(sprAngle*gradToRad);
		length = sqrt((pos.x - By)*(pos.x - By) + (pos.y - Bx)*(pos.y - Bx)) * cos(sprAngle * gradToRad);
		intersect.x = By;
		intersect.y = Bx;
		/*cout << "----------------------" << endl;
		cout << By << " " << Bx << endl;
		cout << pos.x << " " << pos.y << endl;
		cout << "----------------------" << endl;*/
		rayAngle = _myRangle;
	}
}


//void Map::rayHorizontal(const Vector2f & pos, const float & sprAngle, float & rayAngle, Vector2f &intersect, float &length)
//{
//	bool isNull = false;
//	int _myRangle = rayAngle;
//	float Xa = 0, Ya = 0;
//	float gradToRad = 3.14 / 180;
//	int Ax = pos.x, Ay = pos.y;
//	if ((0 < rayAngle) && (rayAngle < 90))
//	{
//		Ax = int(pos.y / _TileSize)*_TileSize - 1;
//		Xa = -_TileSize;
//	}
//	if ((90 < rayAngle) && (rayAngle < 180))
//	{
//		Ax = int(pos.y / _TileSize)*_TileSize - 1;
//		Xa = -_TileSize;
//	}
//	if ((180 < rayAngle) && (rayAngle < 270))
//	{
//		Ax = int(pos.y / _TileSize)*_TileSize + _TileSize;
//		Xa = _TileSize;
//		rayAngle = -rayAngle;
//	}
//	if ((270 < rayAngle) && (rayAngle < 360))
//	{
//		Ax = int(pos.y / _TileSize)*_TileSize + _TileSize;
//		Xa = _TileSize;
//		rayAngle = -rayAngle;
//	}
//
//	/*if (rayAngle == 90)
//	{
//	Ax = pos.x;
//	Xa = 0;
//	Ay = pos.y;
//	Ya = -_TileSize;
//	}
//	else if (rayAngle == 270)
//	{
//	Ax = pos.x;
//	Xa = 0;
//	Ay = pos.y;
//	Ya = _TileSize;
//	}*/
//	if (abs(int(rayAngle)) % 90 == 0)
//	{
//		isNull = true;
//	}
//	else
//	{
//		Ay = pos.x + (pos.y - Ax) / tan(rayAngle*gradToRad);
//		Ya = _TileSize / tan(rayAngle*gradToRad);
//	}
//	if (!isNull)
//	{
//		if ((Ax / _TileSize < 0) || (Ay / _TileSize < 0) || (Ax / _TileSize >= _lairsCount) || (_map[Ax / _TileSize].length() < Ay / _TileSize))
//			isNull = true;
//	}
//	while ((!isNull) && (_map[Ax / _TileSize][Ay / _TileSize] == ' '))
//	{
//		Ax += Xa;
//		Ay += Ya;
//		if ((Ax / _TileSize < 0) || (Ay / _TileSize < 0) || (Ax / _TileSize >= _lairsCount) || (_map[Ax / _TileSize].length() < Ay / _TileSize))
//			isNull = true;
//	}
//	if (isNull)
//	{
//		rayAngle = _myRangle;
//	}
//	else
//	{
//		length = sqrt((pos.x - Ay)*(pos.x - Ay) + (pos.y - Ax)*(pos.y - Ax)) * cos((rayAngle - sprAngle)*gradToRad);
//		intersect.x = Ay;
//		intersect.y = Ax;
//		/*cout << "----------------------" << endl;
//		cout << Ay << " " << Ax << endl;
//		cout << pos.x << " " << pos.y << endl;
//		cout << "----------------------" << endl;*/
//		rayAngle = _myRangle;
//	}
//}

void Map::rayHorizontal(const Vector2f & pos, const float & sprAngle, float & rayAngle, Vector2f &intersect, float &length)
{
	bool isNull = false;
	float tangRay = tan(rayAngle*GradToRad);
	int Ax , Ay ;
	int  Xa, Ya;
	int dy = int(pos.y) % _TileSize;

	if ((0 < rayAngle) && (rayAngle < 90))
	{
		Ax = pos.x + dy / tangRay;
		Xa = _TileSize / tangRay;
		Ay = pos.y - dy - 1;
		Ya = -_TileSize;
	}

	if ((90 < rayAngle) && (rayAngle < 180))
	{
		Ax = pos.x + dy / tangRay;
		Xa = _TileSize / tangRay;
		Ay = pos.y - dy - 1;
		Ya = -_TileSize;
	}

	if ((180 < rayAngle) && (rayAngle < 270))
	{
		Ax = pos.x + -dy / tangRay;
		Xa = -_TileSize / tangRay;
		Ay = pos.y + dy + _TileSize;
		Ya = _TileSize;
	}

	if ((270 < rayAngle) && (rayAngle < 360))
	{
		Ax = pos.x + -dy / tangRay;
		Xa = -_TileSize / tangRay;
		Ay = pos.y + dy + _TileSize;
		Ya = _TileSize;
	}

	if ((rayAngle == 90))
	{
		Ay = pos.y;
		Ya = -_TileSize;
		Ax = pos.x ;
		Xa = 0 ;
	}

	if ( (rayAngle == 270))
	{
		Ay = pos.y ;
		Ya = _TileSize;
		Ax = pos.x;
		Xa = 0;
	}

	int ix = Ax / _TileSize;
	int iy = Ay / _TileSize;

	while (!((ix < 0) || (iy < 0) || (iy >= _lairsCount) || (_map[iy].length() < ix) || (_map[iy][ix] == Wall)))
	{
		Ax += Xa;
		Ay += Ya;
		ix = Ax / _TileSize;
		iy = Ay / _TileSize;

	}
	length = sqrt((pos.x - Ax)*(pos.x - Ax) + (pos.y - Ay)*(pos.y - Ay)) * cos((rayAngle - sprAngle)*GradToRad);
	intersect.x = Ax;
	intersect.y = Ay;
}
void Map::rayOnce(const Vector2f & pos, const float & sprAngle, float & rayAngle, Vector2f & intersect, float & length)
{
	//bool Intersect = false;
	//bool horiz0_vertic1;
	//const float toRad = 3.14 / 180;
	//float x = pos.x;
	//float y = pos.y;
	//float dx = pos.x / 2;
	//float dy = pos.y / 2;
	//float xStep = tan(rayAngle*toRad);
	//float yStep = 1 / tan(rayAngle*toRad);
	//float xIntercept = x  + dx + (-dy / tan(rayAngle*toRad));
	//float yIntercept = y  + dy + (dx / tan(rayAngle*toRad));
	//cout << "y= " << x << endl;
	//cout << "dy= " << dy << endl;
	//cout << "rayAngle= " << rayAngle << endl;
	//cout << " tan(rayAngle*toRad) = " << tan(rayAngle*toRad) << endl;
	//cout << "yIntercept= " << yIntercept << endl;
	//int tileStepX, tileStepY;
	//if ((0 < rayAngle) && (rayAngle < 90))
	//{
	//	tileStepX = 1;
	//	tileStepY = -1;
	//}
	//if ((90 < rayAngle) && (rayAngle < 180))
	//{
	//	tileStepX = -1;
	//	tileStepY = -1;
	//}
	//if ((180 < rayAngle) && (rayAngle < 270))
	//{
	//	tileStepX = -1;
	//	tileStepY = 1;
	//}
	//if ((270 < rayAngle) && (rayAngle < 360))
	//{
	//	tileStepX = 1;
	//	tileStepY = 1;
	//}
	//
	//while (!Intersect)
	//{
	//	
	//	if (_map[abs(int(int(x) / _TileSize))][abs(int(int(yIntercept) / _TileSize))] == '0')
	//	{
	//		Intersect = true;
	//		horiz0_vertic1 = 0;
	//	}
	//	if (_map[abs(int(int(xIntercept) / _TileSize))][abs(int(int(y) / _TileSize))] == '0')
	//	{
	//		Intersect = true;
	//		horiz0_vertic1 = 1;
	//	}
	//	//cout << _map[int(int(x) / _TileSize)][int(int(yIntercept) / _TileSize)] << endl;
	//	cout << _map[abs(int(int(xIntercept) / _TileSize))][abs(int(int(y) / _TileSize))] << endl;
	//	cout << "int(xIntercept) / _TileSize = " << int(xIntercept) / _TileSize << endl;
	//	yIntercept += yStep;
	//	xIntercept += xStep;
	//	y += tileStepY;
	//	x += tileStepX;
	//}
	//switch (horiz0_vertic1)
	//{
	//case 0: {
	//	intersect.x = x - tileStepX;
	//	intersect.y = y + yIntercept - yStep;
	//	break; };
	//case 1: {
	//	intersect.x = x + xIntercept - xStep;
	//	intersect.y = y - tileStepY;
	//	break; };
	//}

	//---------------------------------------

	int length1 = NULL , length2 = NULL;
	Vector2f inters1, inters2;

	///////////////////////////////////////////////////////////////////////////////////

	if ((rayAngle != 0) && (rayAngle != 180))
	{
		rayHorizontal(pos, sprAngle, rayAngle, intersect, length);
		length1 = length;
		inters1 = intersect;
	}

	///////////////////////////////////////////////////////////////////////////////////

	/*rayVertical(pos, sprAngle, rayAngle, intersect, length);
	length2 = length;
	inters2 = intersect;*/


	///////////////////////////////////////////////////////////////////////////////

	//if (length1 == NULL)
	//{
	//	length = length2;
	//	intersect = inters2;
	//	//cout << "Vertical" << endl;
	//}
	//else if (length2 == NULL)
	//{
	//	length = length1;
	//	intersect = inters1;
	//	//cout << "Horizontal" << endl;
	//} 
	//else if (length1 < length2)//�������� ��������� �����������
	//{
	//	length = length1;
	//	intersect = inters1;
	//	//cout << "Horizontal" << endl;
	//}
	//else
	//{
	//	length = length2;
	//	intersect = inters2;
	//	//cout << "Vertical" << endl;
	//}

	//---------------------------------------
	/*float gradToRad = 3.14 / 180;
	intersect.x = pos.x;
	intersect.y = pos.y;
	float Xa, Ya, dX, dY;
	rayAngle = -rayAngle;
	dX = cos(rayAngle*gradToRad);
	dY = sin(rayAngle*gradToRad);
	Xa = dX * _TileSize;
	Ya = dY * _TileSize;

	for (int i = 0; i < 2; i++) 
	{
		while (_map[int(intersect.y) / _TileSize][int(intersect.x) / _TileSize] != Wall)
		{
			intersect.x += Xa;
			intersect.y += Ya;
		}
		if (i == 0) {
			intersect.x -= Xa;
			intersect.y -= Ya;
			Xa = dX;
			Ya = dY;
		}
	}

	length = sqrt((pos.x - intersect.x)*(pos.x - intersect.x) + (pos.y - intersect.y)*(pos.y - intersect.y)) * cos((rayAngle - sprAngle)*gradToRad);
	rayAngle = -rayAngle;*/
}

void Map::rayOnceUnit(const Vector2f &pos, const float &sprAngle, float &rayAngle, Vector2f &intersect, UnitSignature &typeIntersect, float &length)
{
	float gradToRad = 3.14 / 180;
	intersect.x = pos.x;
	intersect.y = pos.y;
	float Xa, Ya;
	rayAngle = -rayAngle;
	Xa = cos(rayAngle*gradToRad);
	Ya = sin(rayAngle*gradToRad);
	while (_map[int(intersect.y) / _TileSize][int(intersect.x) / _TileSize] == Parket)
	{
		intersect.x += Xa;
		intersect.y += Ya;

	}
	length = sqrt((pos.x - intersect.x)*(pos.x - intersect.x) + (pos.y - intersect.y)*(pos.y - intersect.y)) * cos((rayAngle - sprAngle)*gradToRad);
	typeIntersect = UnitSignature(_map[int(intersect.y) / _TileSize][int(intersect.x) / _TileSize]);
	rayAngle = -rayAngle;
}

void Map::showUnit3D(const int & xPos, const float & length, RenderWindow & window3D , UnitSignature &typeIntersect)
{
	if ((typeIntersect != Parket)&&(typeIntersect != Wall))
	{
		sf::VertexArray line(sf::Lines, 2);
		for (int i = xPos * 10; i < xPos * 10 + 10; i++)
		{
			line[0].position = sf::Vector2f({ float(i) , _windowHeight / 2 });
			line[0].color = Color(255, typeIntersect * 50, 0);
			line[1].position = sf::Vector2f({ float(i) , _windowHeight / 2 + length / 2 });
			line[1].color = Color(255, typeIntersect * 50, 0);
			window3D.draw(line);
		}
	}
}

void Map::window3Dshow(const int & xPos, const float & length , RenderWindow &window3D)
{
	sf::VertexArray line(sf::Lines, 2);
	for (int i = xPos * 10; i < xPos * 10 + 10; i++)
	{
		line[0].position = sf::Vector2f({float(i) , _windowHeight / 2 - length / 2 });
		//line[0].color = Color(int(length) % 255 , int(length) % 255 , int(length) % 255  );
		line[1].position = sf::Vector2f({float(i) , _windowHeight / 2 + length / 2 });
		line[1].color = Color(int(length) % 255 , int(length) % 150 + 100, int(length) % 150 + 100);
		window3D.draw(line);
	}

	line[0].position = sf::Vector2f({ float(xPos * 10) , _windowHeight / 2 - length / 2 });
	line[0].color = Color(0,0,0);
	line[1].position = sf::Vector2f({ float(xPos * 10 + 10) , _windowHeight / 2 - length / 2 });
	line[1].color = Color(0, 0, 0);
	window3D.draw(line);

	line[0].position = sf::Vector2f({ float(xPos * 10) , _windowHeight / 2 + length / 2 });
	line[0].color = Color(0, 0, 0);
	line[1].position = sf::Vector2f({ float(xPos * 10 + 10) , _windowHeight / 2 + length / 2 });
	line[1].color = Color(0, 0, 0);
	window3D.draw(line);
}

void Map::window2Dshow(const Vector2f & pos, const Vector2f & intersect)
{
	sf::VertexArray line(sf::Lines, 2);

	line[0].position = pos;
	line[1].position = intersect;
	_window->draw(line);
}

void Map::rayCast(RenderWindow &window3D,const Vector2f & pos,const float & sprAngle)
{
	float rayAngle = -int(sprAngle) + 30;
	float length = 0;
	Vector2f intersect;
	UnitSignature typeIntersect;
	
	

	for (int i = 0; i < _windowWidth / 10 ; i++)
	{
		if (rayAngle < 0)rayAngle += 360;
		if (rayAngle > 360)rayAngle -= 360;

		rayOnce(pos, sprAngle , rayAngle , intersect, length);
		window2Dshow(pos, intersect);
		length = _sizeConst / length;
		window3Dshow( i, length, window3D);

		/*rayOnceUnit(pos, sprAngle, rayAngle, intersect, typeIntersect, length);
		window2Dshow(pos, intersect);
		length = _sizeConst / length;
		showUnit3D(i, length, window3D , typeIntersect);*/

		rayAngle -= _dAngle;

	}
	
	lines(pos, sprAngle);
}



int Map::getEnemyCount()
{
	return _enemyCount;
}

int Map::iterWithPlayer(Vector2f pos)
{
	int cross = 1;
	for (int i = int(pos.x / _TileSize); i < int((pos.x + _TileSize) / _TileSize); i++)
		for (int j = int(pos.y / _TileSize); j < int((pos.y + _TileSize) / _TileSize); j++)
		{
			if ((_map[j][i] != '0'))
			{
				cross = -1;
			}
		}
	return cross;
}


bool Map::checkPos(Vector2f pos , Vector2i unitSize)
{
	for (int i = int(pos.x / _TileSize); i < int((pos.x + _TileSize * unitSize.x) / _TileSize); i++)
		for (int j = int(pos.y / _TileSize); j < int((pos.y + _TileSize * unitSize.y) / _TileSize); j++)
		{
			if ((_map[j][i] != ' '))
			{
				return false;
			}
		}
	return true;
}
void Map::putUnit(Vector2i posMap, UnitSignature unitSignature)
{
	//if ((_map[posMap.x][posMap.y] != Wall))
		_map[posMap.y][posMap.x] = unitSignature;
	

}

