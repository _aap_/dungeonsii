#pragma once
#include<SFML\Graphics.hpp>
#include"consts.h"
using namespace sf;

class Map3D
{
private:
	RenderWindow *_window3D;
	MapType _rayMap;
public:
	Map3D(RenderWindow *window3D);
	void setMap(const MapType &map);
	void rayCast(const Vector2f &pos ,const int angle);
	~Map3D();
};

